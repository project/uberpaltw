; Drupal Core
api = 2
core = 6.25

; Modules
projects[admin_menu][subdir] = modules
projects[admin_menu][type] = module
projects[admin_menu][version] = 1.8

projects[cck][subdir] = modules
projects[cck][type] = module
projects[cck][version] = 2.9

projects[filefield][subdir] = modules
projects[filefield][type] = module
projects[filefield][version] = 3.10

projects[imageapi][subdir] = modules
projects[imageapi][type] = module
projects[imageapi][version] = 1.10

projects[imagecache][subdir] = modules
projects[imagecache][type] = module
projects[imagecache][version] = 2.0-beta12

projects[imagefield][subdir] = modules
projects[imagefield][type] = module
projects[imagefield][version] = 3.10

projects[poormanscron][subdir] = modules
projects[poormanscron][type] = module
projects[poormanscron][version] = 2.2

projects[skinr][subdir] = modules
projects[skinr][type] = module
projects[skinr][version] = 1.6

projects[thickbox][subdir] = modules
projects[thickbox][type] = module
projects[thickbox][version] = 1.6

projects[token][subdir] = modules
projects[token][type] = module
projects[token][version] = 1.18

projects[ubercart][subdir] = modules
projects[ubercart][type] = module
projects[ubercart][version] = 2.7
projects[ubercart][patch][] = http://drupal.org/files/uberpaltw-fix-ubercart-6.x-2.7-shipping.patch

projects[views][subdir] = modules
projects[views][type] = module
projects[views][version] = 2.16

; Themes
projects[acquia_prosper][subdir] = themes
projects[acquia_prosper][type] = theme
projects[acquia_prosper] = 1.1

projects[fusion][subdir] = themes
projects[fusion][type] = theme
projects[fusion] = 1.12
