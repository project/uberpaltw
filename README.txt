This profile will install Drupal 6.x with Ubercart 2.x and PayPal Express Checkout in Traditional-Chinese.
Project is sponsored by PayPal and developed by NETivism.

Installation
--------------
1. Download Chinese Traditional translations
   You can download better core and ubercart translations from http://p.netivism.com.tw/installation
   Ubercart translations you must into ubercart/ca/translations
  1.1. Core translations into profiles/uberpaltw/translations and modules/system/translations
  1.2. All modules translations into profiles/uberpaltw/modules/[module name]/translations
2. Set store information and paypal information when install
3. Paypal API Server default set to "Live", if want to test, please set to "Sandbox" on admin/store/settings/payment/edit/gateways

Patch
--------
For fix shipping bug on paypal (Only express checkout): http://drupal.org/files/uberpaltw-fix-ubercart-6.x-2.7-shipping.patch

NVP API Method and Field Reference
-------------------------------------
https://www.paypalobjects.com/en_US/ebook/PP_NVPAPI_DeveloperGuide/Appx_fieldreference.html

Paypal pending message
-------------------------
The "multi-currency" return code is "multi_currency" on uc_paypal.module

Paypal SetExpressCheckout parameter
--------------------------------------
Add total amount information on paypal description

Demo site
-----------
http://p.netivism.com.tw/

Discuss
---------
http://drupaltaiwan.org/forum/20111226/6011
