<?php
// $Id: uberpaltw.profile,v 1.3.2.4 2010/08/20 15:51:29 rszrama Exp $

/**
 * @file
 * Ubercart installation profile.
 */

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile,
 *   and optional 'language' to override the language selection for
 *   language-specific profiles, e.g., 'language' => 'fr'.
 */
function uberpaltw_profile_details() {
  return array(
    'name' => 'Ubercart x PayPal x Taiwan',
    'description' => 'Install Drupal with Ubercart and Paypal module in Traditional-Chinese.'
  );
}

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * The following required core modules are always enabled:
 * 'block', 'filter', 'node', 'system', 'user'.
 *
 * @return
 *  An array of modules to be enabled.
 */
function uberpaltw_profile_modules() {
  $plugin = class_exists('imagick') ? 'imageapi_imagemagick' : 'imageapi_gd';

  return array(
    // Core module
    'comment',
    'dblog',
    'help',
    'locale',
    'menu',
    'path',
    'taxonomy',
    'book',

    // Common module
    'token',
    'imageapi',
    $plugin,
    'imagecache',
    'imagecache_ui',
    'content',
    'content_copy',
    'fieldgroup',
    'text',
    'filefield',
    'imagefield',
    'views',
    'views_ui',
    'admin_menu',
    'thickbox',
    'poormanscron',

    // Ubercart core module
    'ca',
    'uc_order',
    'uc_product',
    'uc_cart',
    'uc_store',

    // Ubercart payment module
    'uc_payment',
    'uc_credit',
    'uc_paypal',

    // Ubercart other module
    'uc_catalog',
  );
}

/**
 * Return a list of tasks that this profile supports.
 *
 * @return
 *   A keyed array of tasks the profile will perform during the final stage. The
 *   keys of the array will be used internally, while the values will be
 *   displayed to the user in the installer task list.
 */
function uberpaltw_profile_task_list() {
  return array(
    'config' => st('Configure store'),
    'payment' => st('Configure payment'),
    'demo' => st('Install demo'),
  );
}

/**
 * Perform installation tasks for this installation profile.
 */
function uberpaltw_profile_tasks(&$task, $url) {
  switch ($task) {
    // Perform tasks when the installation form is initially submitted and all
    // the specified modules have been installed.
    case 'profile':
      // Default node comments disabled.
      variable_set('comment_book', COMMENT_NODE_DISABLED);
      variable_set('comment_product', COMMENT_NODE_DISABLED);

      // Toggle the node info display.
      $settings = variable_get('theme_settings', array());
      $settings['toggle_node_info_book'] = 0;
      $settings['toggle_node_info_product'] = 0;
      variable_set('theme_settings', $settings);

      // Use a working store dashboard setting.
      variable_set('uc_store_admin_page_display', 4);

      // Turn off the Ubercart store footer.
      variable_set('uc_footer_message', 'none');

      // Set currency code for taiwan
      variable_set('uc_currency_code', 'TWD');

      // Set currency sign for taiwan
      variable_set('uc_currency_sign', '(TWD)');

      // Set currency sign after amount
      variable_set('uc_sign_after_amount', true);

      // Set API Server to Live
      variable_set('uc_paypal_wpp_server', 'https://api-3t.paypal.com/nvp');

      // Set button source to netivism
      variable_set('uc_paypal_ec_button_source', 'Netvism_Cart_EC');
      variable_set('uc_paypal_wps_button_source', 'Netvism_Cart_EC');      

      // Set payment message
      variable_set('uc_default_payment_msg', 'PayPal 是更安全、快速、簡單的跨國線上交易支付平台。這項服務讓全球買家與賣家自由選擇合適的支付購物款項方式，包含信用卡、扣帳卡、銀行帳戶等，無需透露財務資料給他人。');

      // Set decimal places to zero
      variable_set('uc_currency_prec', 0);

      // Set weight unit to kg
      variable_set('uc_weight_unit', 'kg');

      // Set length unit to cm
      variable_set('uc_length_unit', 'cm');

      // Set data format for taiwan
      variable_set('uc_date_format_default', 'Y/m/d');

      // Set image widget
      variable_set('uc_product_image_widget', 'thickbox');

      // Set front page, catalog will rewrite this in uberpaltw_setup_views
      variable_set('site_frontpage', 'products');

      // Install Acquia Prosper theme
      uberpaltw_install_theme();

      // Set and add taxonomy for catalog
      uberpaltw_setup_taxonomy();

      // Create new views and set front page
      uberpaltw_setup_views();

      // Create default book page
      uberpaltw_setup_book();

      // Configure the blocks
      uberpaltw_setup_blocks();

      // Add admin role and setup default permission
      uberpaltw_setup_user();

      // Setup default menu items
      uberpaltw_setup_menu();

      // Update the menu router information.
      menu_rebuild();

      // Import every country available.
      require_once drupal_get_path('module', 'uc_store') .'/uc_store.admin.inc';

      // Get an array of all the files in the countries directory.
      $files = _country_import_list();

      // Unset any countries from the files array that have already been imported.
      $result = db_query("SELECT * FROM {uc_countries} ORDER BY country_name ASC");
      while ($country = db_fetch_object($result)) {
        unset($files[$country->country_id]);
      }

      // Install any country remaining in the files array.
      foreach ($files as $file) {
        uc_country_import($file['file']);
      }

      // Set $task to next task so the UI will be correct.
      $task = 'config';
      drupal_set_title(st('Configure store'));
      return drupal_get_form('uberpaltw_store_settings_form', $url);

    // Perform tasks when the store configuration form has been submitted.
    case 'config':
      // Save the values from the store configuration form.
      uberpaltw_store_settings_form_submit();

      // Set default currency code as TWD
      variable_set('uc_paypal_wpp_currency', 'TWD');

      // Enable PayPal Express Checkout method
      variable_set('uc_payment_method_paypal_ec_checkout', true);

      // Disable Credit card method
      variable_set('uc_payment_method_credit_checkout', false);

      // Move to the payment installation task.
      $task = 'payment';
      drupal_set_title(st('Configure payment'));
      return drupal_get_form('uberpaltw_payment_form', $url);

    // Perform tasks when the payment selection form has been submitted.
    case 'payment':
      // Save the values from the payment configuration form.
      uberpaltw_payment_form_submit();

      // Move to the demo installation task.
      $task = 'demo';
      drupal_set_title(st('Install demo'));
      return drupal_get_form('uberpaltw_demo_form', $url);

    // Perform tasks when the demo selection form has been submitted.
    case 'demo':
      // Install the selected demo at this time.
      uberpaltw_demo_form_submit();

      // Move to the completion task.
      $task = 'profile-finished';
      break;
  }
}

/**
 * Build the Ubercart store configuration form.
 *
 * @param $form_state
 * @param $url
 *   URL of current installer page, provided by installer.
 */
function uberpaltw_store_settings_form(&$form_state, $url) {
  $form = array(
    '#action' => $url,
    '#redirect' => FALSE,
  );

  // Add the store contact information to the form.
  $form['uc_store_contact_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact information'),
  );

  $form['uc_store_contact_info']['uc_store_name'] = uc_textfield(t('Store name'), variable_get('site_name', NULL), FALSE, NULL, 64);
  $form['uc_store_contact_info']['uc_store_owner'] = uc_textfield(t('Store owner'), NULL, FALSE, NULL, 64);

  $form['uc_store_contact_info']['uc_store_phone'] = uc_textfield(t('Phone number'), NULL, FALSE);
  $form['uc_store_contact_info']['uc_store_fax'] = uc_textfield(t('Fax number'), NULL, FALSE);

  $form['uc_store_contact_info']['uc_store_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#description' => NULL,
    '#size' => 32,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $form['uc_store_contact_info']['uc_store_email_include_name'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include the store name in the from line of store e-mails.'),
    '#description' => t('May not be available on all server configurations. Turn off if this causes problems.'),
    '#default_value' => TRUE,
  );

  $form['uc_store_address'] = array(
    '#type' => 'fieldset',
    '#title' => t('Store address'),
  );

  // Set default country to taiwan (158)
  variable_set('uc_store_country', 158);

  $form['uc_store_address']['uc_store_country'] = uc_country_select(uc_get_field_name('country'), uc_store_default_country());
  $form['uc_store_address']['uc_store_postal_code'] = uc_textfield(uc_get_field_name('postal_code'), NULL, FALSE, NULL, 10);
  $country_id = uc_store_default_country();
  $form['uc_store_address']['uc_store_zone'] = uc_zone_select(uc_get_field_name('zone'), NULL, NULL, $country_id);
  $form['uc_store_address']['uc_store_city'] = uc_textfield(uc_get_field_name('city'), NULL, FALSE);
  $form['uc_store_address']['uc_store_street1'] = uc_textfield(uc_get_field_name('street1'), NULL, FALSE, NULL, 128);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
  );

  return $form;
}
function uberpaltw_store_settings_form_submit() {
  $form_state = array('values' => $_POST);
  system_settings_form_submit(array(), $form_state);
}

/**
 * Build the Ubercart payment configuration form.
 *
 * @param $form_state
 * @param $url
 *   URL of current installer page, provided by installer.
 */
function uberpaltw_payment_form(&$form_state, $url) {
  $form = array(
    '#action' => $url,
    '#redirect' => FALSE,
  );

  $form['uc_paypal_wps_email'] = array(
    '#type' => 'textfield',
    '#title' => t('PayPal e-mail address'),
    '#description' => t('<a href="!url">Do not have PayPal account? Register now.</a>', array('!url' => 'http://paypal.com/tw')),
  );
  $form['uc_paypal_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('API username'),
    '#description' => t('<a href="!url">How to get API?</a>', array('!url' => 'http://p.netivism.com.tw/installation/install/paypal-api')),
  );
  $form['uc_paypal_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('API password'),
  );
  $form['uc_paypal_api_signature'] = array(
    '#type' => 'textfield',
    '#title' => t('Signature'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
  );

  return $form;
}
function uberpaltw_payment_form_submit() {
  $form_state = array('values' => $_POST);
  system_settings_form_submit(array(), $form_state);
}

/**
 * Build the Ubercart demo selection form.
 *
 * @param $form_state
 * @param $url
 *   URL of current installer page, provided by installer.
 */
function uberpaltw_demo_form($form_state, $url) {
  $form = array(
    '#action' => $url,
    '#redirect' => FALSE,
  );

  $form['product'] = array(
    '#type' => 'checkbox',
    '#title' => st('Demo products'),
    '#description' => st('Install some default products.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Install demo'),
  );

  return $form;
}
function uberpaltw_demo_form_submit() {
  // Install product demo
  if ($_POST['product']) {
    $uc_catalog_vid = variable_get('uc_catalog_vid', '');
    $uc_weight_unit = variable_get('uc_weight_unit', 'kg');
    $uc_length_unit = variable_get('uc_length_unit', 'cm');

    $new_products = db_fetch_object(db_query("SELECT tid FROM {term_data} WHERE vid=%d AND name='%s' LIMIT 1", $uc_catalog_vid, t('New Products')));
    $special_offers = db_fetch_object(db_query("SELECT tid FROM {term_data} WHERE vid=%d AND name='%s' LIMIT 1", $uc_catalog_vid, t('Special offers')));

    $nodes = array();
    $nodes[] = array(
      'title' => '大面神禮盒',
      'body' => '喜願麵包坊歷經四年的本土小麥契作行動，串起更多小農一同努力，當前端的農友、生產者熱情投入，後端的消費支持就是最好的回應。

第一波的本土小麥製品以麵條為主角。由於麵條是最貼近常民的食物，在食用調理上與保存上沒有距離和壓力。「麵條」耐保存，以當季鮮食結合計畫消費的預購制，可以有效緩解收成期第一時間的儲存壓力。新鮮、自然就是本土小麥最好的競爭力，本季收成的小麥化身為大麵神禮盒，除了主角麵條，還有麵粉、麵茶、和涼（拌）醬料，方便大家日常使用。

「大面（麵）神」即閩南話的「厚臉皮」的意思，亦即在這個展業的過程，我們必須勇於走入群眾，勇於推廣台灣在地食材。誠摯邀請各位朋友一起來當大麵神，除了自家購買食用，更是餽贈分享親友的台灣好禮。',
      'list_price' => 700,
      'cost' => 700,
      'sell_price' => 660,
      'weight' => 0,
      'weight_units' => $uc_weight_unit,
      'sku' => 'product1',
      'catalog' => 'special',
      'images' => array('product1-1.jpg', 'product1-2.jpg', 'product1-3.jpg', 'product1-4.jpg', 'product1-5.jpg'),
    );
    $nodes[] = array(
      'title' => '喜願全麥麵條',
      'body' => '喜願全麥麵條使用百分之百的台灣本土小麥，將百分之五十一新鮮的全麥粒研磨，完整保留全麥營養，另外百分之四十九搭配喜願白海豚中筋麵粉，呈現出不同於坊間白麵條的勁道，因此在烹煮時, 隨著熱氣散漫出濃郁的麥香。

喜願小麥在台灣

要 讓台灣小麥重現江湖是一件不容易的事，除了不易找到有意願配合契作的農友，在種植過程中也是困難重重，因為種植小麥的理想季節為二期稻作後，田野間幾乎少 有榖類的作物，若此時出現小面積的麥田，將成為小鳥的糧倉而影響收成。在「小麥」收割時已經去殼成為「小麥籽」，烘乾後若不加工為成品（如麵包、包子饅 頭、餅乾、麵條..），就必須存放於低溫低濕庫中；若採室內儲存，以台灣悶熱潮濕的氣候，小麥不出兩個月就會出現蛀蟲。因此小麥契作必須做好收成的後製工 序，包括研磨、加工、儲存、包裝、運輸的細節計畫，在在的都影響了喜願小麥在收成上的成果。',
      'list_price' => 50,
      'cost' => 40,
      'sell_price' => 50,
      'weight' => 0,
      'weight_units' => $uc_weight_unit,
      'sku' => 'product2',
      'catalog' => 'special',
      'images' => array('product2-1.jpg', 'product2-2.jpg', 'product2-3.jpg', 'product2-4.jpg', 'product2-5.jpg'),
    );
    $nodes[] = array(
      'title' => '台灣傳統全麥麵茶',
      'body' => '純素、無油、無糖、無人工添加物，天然焙炒。
本品可中熱水即食，可搭配其他穀物粉沖泡食用。
可與奶油與草本植物併炒，可作為各式濃湯油酥。
可作為炸物裹粉之用，或拌蛋汁、水使用。
酌量加入麵包、餅乾、蛋糕可增進濃郁麥香風味。',
      'list_price' => 70,
      'cost' => 49,
      'sell_price' => 70,
      'weight' => 300,
      'weight_units' => 'g',
      'sku' => 'product3',
      'catalog' => 'new',
      'images' => array('product3-1.jpg', 'product3-2.jpg', 'product3-3.jpg', 'product3-4.jpg', 'product3-5.jpg'),
    );
    $nodes[] = array(
      'title' => '喜願白海豚中筋麵粉',
      'body' => '喜願白海豚中筋麵粉，使用契作本土小麥台中選二號研磨，粉質細緻色度勻白，研磨後不經後製配粉，總粉直接扮合，靜置熟成，新鮮看得見聞得到。適合做白吐司、法國麵包、包子饅頭等中式麵點，是喜愛自己動手做烘焙糕點的首選。',
      'list_price' => 100,
      'cost' => 70,
      'sell_price' => 100,
      'weight' => 1.2,
      'weight_units' => $uc_weight_unit,
      'sku' => 'product4',
      'catalog' => 'new',
      'images' => array('product4-1.jpg', 'product4-2.jpg', 'product4-3.jpg', 'product4-4.jpg', 'product4-5.jpg'),
    );
    $nodes[] = array(
      'title' => '台灣本土全麥粉',
      'body' => '可熱炒為麵茶與其他穀物粉沖泡食用，也可以和水成麵糊，經川燙後煮麵疙瘩，亦可將麵糊加入熱炒爆香後的蔬菜，加油熱鍋煎成 蔬菜餅。若加入包子、麵包、糕點…中建議比例（對粉量）在15-25%，依個人操作習慣漸次調高。低溫全麥粒研磨，保留小麥完整營養，小麥會因種植區域不 同研磨色澤會略有不同，敬請理解。',
      'list_price' => 100,
      'cost' => 70,
      'sell_price' => 100,
      'weight' => 1.2,
      'weight_units' => $uc_weight_unit,
      'sku' => 'product5',
      'catalog' => 'new',
      'images' => array('product5-1.jpg', 'product5-2.jpg', 'product5-3.jpg', 'product5-4.jpg'),
    );

    $source_path = 'profiles/uberpaltw/images/';
    $file_path = file_directory_path() . '/';

    foreach ($nodes as $item) {
      $node = new stdClass();
      $node->type = 'product';
      $node->title = $item['title'];
      $node->body = $item['body'];
      $node->format = 1;
      $node->created = time();
      $node->changed = time();
      $node->uid = 1;
      $node->status = 1;
      $node->promote = 1;
      $node->sticky = 0;
      $node->language = 'zh-hant';

      // product
      if ($new_products && $item['catalog'] == 'new') {
        $node->taxonomy = array($new_products->tid);
      }
      if ($special_offers && $item['catalog'] == 'special') {
        $node->taxonomy = array($special_offers->tid);
      }
      $node->model = $item['sku'];
      $node->list_price = $item['list_price'];
      $node->cost = $item['cost'];
      $node->sell_price = $item['sell_price'];
      $node->shippable = 1;
      $node->weight = $item['weight'];
      $node->weight_units = $item['weight_units'];
      $node->length_units = $uc_length_unit;
      $node->pkg_qty = 1;
      $node->default_qty = 1;

      node_save($node);

      if (db_table_exists('content_field_image_cache')) {
        // get node id
        $node_item = db_fetch_object(db_query("SELECT nid, vid FROM {node} ORDER BY nid DESC LIMIT 1"));

        // set file data array
        $file_data = array('alt' => $item['title'], 'title' => $item['title']);
        $file_data = serialize($file_data);

        $delta = 0;
        foreach ($item['images'] as $item_image) {
          // create product picture
          $source_link = $source_path . $item_image;
          file_copy($source_link, $file_path . $item_image, FILE_EXISTS_REPLACE);
          db_query("INSERT INTO {files} (uid, filename, filepath, filemime, filesize, status, timestamp) VALUES (%d, '%s', '%s', '%s', %d, %d, %d)", 1, $item_image, $file_path . $item_image, 'image/jpeg', filesize($file_path . $item_image), 1, time());
          $fid = db_last_insert_id('files', 'fid');

          db_query("REPLACE INTO {content_field_image_cache} (vid, nid, delta, field_image_cache_fid, field_image_cache_list, field_image_cache_data) VALUES (%d, %d, %d, %d, %d, '%s')", $node_item->vid, $node_item->nid, $delta, $fid, 1, $file_data);

          $delta++;
        }
      }  // Create product picture
    }
  }
}

/**
 * Install a module or array of modules if the modules exist in the site.
 *
 * @param $module
 *   A module name or array of module names to install if the modules exist.
 * @return
 *   Returns TRUE or FALSE to indicate whether a module actually existed; no
 *     return value for multiple installations by array.
 */
function uberpaltw_install_module($module) {
  // If an array was passed...
  if (is_array($module)) {
    // Loop through the array and install each one.
    foreach ($module as $mod) {
      uberpaltw_install_module($mod);
    }
  }
  else {
    // Otherwise go ahead and attempt the install.
    if (drupal_get_path('module', $module)) {
      drupal_load('module', $module);
      drupal_install_modules(array($module));
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}

/**
 * Install Acquia Prosper theme
 */
function uberpaltw_install_theme() {
  $themes = array_keys(system_theme_data());

  if (drupal_get_path('module', 'skinr') && in_array('fusion_core', $themes) && in_array('acquia_prosper', $themes)) {
    // First install Skinr.
    uberpaltw_install_module('skinr');

    // Then install the themes.
    db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' AND (name = '%s' OR name = '%s')", 'fusion_core', 'acquia_prosper');

    // And finally set Acquia Prosper to be the default.
    variable_set('theme_default', 'acquia_prosper');
  }
}

/**
 * Edit catalog taxonomy and add term
 */
function uberpaltw_setup_taxonomy() {
  global $tid_new_products, $tid_special_offers;

  $catalog_vid = variable_get('uc_catalog_vid', '');
  if (!empty($catalog_vid)) {
    // Set vocabulary settings
    db_query("UPDATE {vocabulary} SET name='%s', required=1 WHERE vid=%d LIMIT 1", t('Catalog'), $catalog_vid);

    // Add default term
    $term = array(
      'vid' => $catalog_vid,
      'name' => t('New Products'),
    );
    taxonomy_save_term($term);

    $term = array(
      'vid' => $catalog_vid,
      'name' => t('Special offers'),
    );
    taxonomy_save_term($term);

    // Insert catalog image
    if (db_table_exists('uc_catalog_images')) {
      $file_path = file_directory_path() . '/';

      $term = db_fetch_object(db_query("SELECT tid FROM {term_data} WHERE vid=%d AND name='%s' LIMIT 1", $catalog_vid, t('New Products')));
      if ($term) {
        $tid_new_products = $term->tid;

        $source_link = 'profiles/uberpaltw/images/new-products.jpg';
        file_copy($source_link, $file_path . 'new-products.jpg', FILE_EXISTS_REPLACE);
        db_query("INSERT INTO {files} (uid, filename, filepath, filemime, filesize, status, timestamp) VALUES (%d, '%s', '%s', '%s', %d, %d, %d)", 1, 'new-products.jpg', $file_path . 'new-products.jpg', 'image/jpeg', filesize($file_path . 'new-products.jpg'), 1, time());
        $fid = db_last_insert_id('files', 'fid');
        db_query("INSERT INTO {uc_catalog_images} (fid, tid, filename, filepath, filemime, filesize) VALUES (%d, %d, '%s', '%s', '%s', %d)", $fid, $term->tid, 'new-products.jpg', $file_path . 'new-products.jpg', 'image/jpeg', filesize($file_path . 'new-products.jpg'));
      }

      $term = db_fetch_object(db_query("SELECT tid FROM {term_data} WHERE vid=%d AND name='%s' LIMIT 1", $catalog_vid, t('Special offers')));
      if ($term) {
        $tid_special_offers = $term->tid;

        $source_link = 'profiles/uberpaltw/images/special-offers.jpg';
        file_copy($source_link, $file_path . 'special-offers.jpg', FILE_EXISTS_REPLACE);
        db_query("INSERT INTO {files} (uid, filename, filepath, filemime, filesize, status, timestamp) VALUES (%d, '%s', '%s', '%s', %d, %d, %d)", 1, 'special-offers.jpg', $file_path . 'special-offers.jpg', 'image/jpeg', filesize($file_path . 'special-offers.jpg'), 1, time());
        $fid = db_last_insert_id('files', 'fid');
        db_query("INSERT INTO {uc_catalog_images} (fid, tid, filename, filepath, filemime, filesize) VALUES (%d, %d, '%s', '%s', '%s', %d)", $fid, $term->tid, 'special-offers.jpg', $file_path . 'special-offers.jpg', 'image/jpeg', filesize($file_path . 'special-offers.jpg'));
      }
    }
  }
}

/**
 * Create new views and set front page
 */
function uberpaltw_setup_views() {
  global $tid_new_products, $tid_special_offers;
  $catalog_vid = variable_get('uc_catalog_vid', '');

  // Create imagecache for homepage
  $preset = imagecache_preset_save(array('presetname' => 'product_home'));
  $action = array(
    'presetid' => $preset['presetid'],
    'weight' => 0,
    'module' => 'imagecache',
    'action' => 'imagecache_scale_and_crop',
    'data' => array(
      'width' => 360,
      'height' => 240,
    ),
  );
  imagecache_action_save($action);

  // Rewrite imagecache for product
  $preset = imagecache_preset_save(array('presetname' => 'product'));
  $action = array(
    'presetid' => $preset['presetid'],
    'weight' => 0,
    'module' => 'imagecache',
    'action' => 'imagecache_scale_and_crop',
    'data' => array(
      'width' => 220,
      'height' => 220,
    ),
  );
  imagecache_action_save($action);

  if (!empty($catalog_vid) && !empty($tid_new_products) && !empty($tid_special_offers)) {
    // Create views object
    views_include('view');

    $view = new view;
    $view->name = 'products_home';
    $view->description = 'Products for front page.';
    $view->tag = '';
    $view->view_php = '';
    $view->base_table = 'node';
    $view->is_cacheable = FALSE;
    $view->api_version = 2;
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->override_option('fields', array(
      'title' => array(
        'label' => '',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_node' => 1,
        'exclude' => 0,
        'id' => 'title',
        'table' => 'node',
        'field' => 'title',
        'relationship' => 'none',
        'override' => array(
          'button' => t('Override'),
        ),
      ),
      'field_image_cache_fid' => array(
        'label' => '',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_node' => 1,
        'label_type' => 'none',
        'format' => 'product_default',
        'multiple' => array(
          'group' => 0,
          'multiple_number' => '1',
          'multiple_from' => '0',
          'multiple_reversed' => 0,
        ),
        'exclude' => 0,
        'id' => 'field_image_cache_fid',
        'table' => 'node_data_field_image_cache',
        'field' => 'field_image_cache_fid',
        'relationship' => 'none',
        'override' => array(
          'button' => t('Override'),
        ),
      ),
      'sell_price' => array(
        'label' => t('Sell price'),
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'set_precision' => 0,
        'precision' => '0',
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'format' => 'uc_price',
        'revision' => 'themed',
        'exclude' => 0,
        'id' => 'sell_price',
        'table' => 'uc_products',
        'field' => 'sell_price',
        'relationship' => 'none',
        'override' => array(
          'button' => t('Override'),
        ),
      ),
      'buyitnowbutton' => array(
        'label' => '',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'exclude' => 0,
        'id' => 'buyitnowbutton',
        'table' => 'uc_products',
        'field' => 'buyitnowbutton',
        'relationship' => 'none',
        'override' => array(
          'button' => t('Override'),
        ),
      ),
    ));
    $handler->override_option('sorts', array(
      'created' => array(
        'order' => 'DESC',
        'granularity' => 'second',
        'id' => 'created',
        'table' => 'node',
        'field' => 'created',
        'override' => array(
          'button' => t('Override'),
        ),
        'relationship' => 'none',
      ),
    ));
    $handler->override_option('filters', array(
      'status' => array(
        'operator' => '=',
        'value' => 1,
        'group' => '0',
        'exposed' => FALSE,
        'expose' => array(
          'operator' => FALSE,
          'label' => '',
        ),
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'relationship' => 'none',
      ),
      'is_product' => array(
        'operator' => '=',
        'value' => 1,
        'group' => '0',
        'exposed' => FALSE,
        'expose' => array(
          'operator' => FALSE,
          'label' => '',
        ),
        'id' => 'is_product',
        'table' => 'uc_products',
        'field' => 'is_product',
        'relationship' => 'none',
      ),
      'tid' => array(
        'operator' => 'or',
        'value' => array(
          0 => $tid_new_products,
        ),
        'group' => '0',
        'exposed' => FALSE,
        'expose' => array(
          'operator' => FALSE,
          'label' => '',
        ),
        'type' => 'textfield',
        'limit' => TRUE,
        'vid' => $catalog_vid,
        'id' => 'tid',
        'table' => 'term_node',
        'field' => 'tid',
        'hierarchy' => 0,
        'relationship' => 'none',
        'reduce_duplicates' => 0,
      ),
    ));
    $handler->override_option('access', array(
      'type' => 'none',
      'role' => array(),
      'perm' => '',
    ));
    $handler->override_option('cache', array(
      'type' => 'none',
    ));
    $handler->override_option('items_per_page', 6);
    $handler->override_option('use_pager', '0');
    $handler->override_option('distinct', 1);
    $handler->override_option('style_plugin', 'grid');
    $handler->override_option('style_options', array(
      'grouping' => '',
      'columns' => '3',
      'alignment' => 'horizontal',
      'fill_single_line' => 1,
    ));
    $handler = $view->new_display('page', t('Page'), 'page_1');
    $handler->override_option('fields', array(
      'title' => array(
        'label' => '',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_node' => 1,
        'exclude' => 0,
        'id' => 'title',
        'table' => 'node',
        'field' => 'title',
        'relationship' => 'none',
        'override' => array(
          'button' => t('Override'),
        ),
      ),
      'field_image_cache_fid' => array(
        'label' => '',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_node' => 1,
        'label_type' => 'none',
        'format' => 'product_home_default',
        'multiple' => array(
          'group' => 0,
          'multiple_number' => '1',
          'multiple_from' => '0',
          'multiple_reversed' => 0,
        ),
        'exclude' => 0,
        'id' => 'field_image_cache_fid',
        'table' => 'node_data_field_image_cache',
        'field' => 'field_image_cache_fid',
        'relationship' => 'none',
        'override' => array(
          'button' => t('Use default'),
        ),
      ),
      'sell_price' => array(
        'label' => t('Sell price'),
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'set_precision' => 0,
        'precision' => '0',
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'format' => 'uc_price',
        'revision' => 'themed',
        'exclude' => 0,
        'id' => 'sell_price',
        'table' => 'uc_products',
        'field' => 'sell_price',
        'relationship' => 'none',
        'override' => array(
          'button' => t('Override'),
        ),
      ),
      'buyitnowbutton' => array(
        'label' => '',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'exclude' => 0,
        'id' => 'buyitnowbutton',
        'table' => 'uc_products',
        'field' => 'buyitnowbutton',
        'relationship' => 'none',
        'override' => array(
          'button' => t('Override'),
        ),
      ),
    ));
    $handler->override_option('filters', array(
      'status' => array(
        'operator' => '=',
        'value' => 1,
        'group' => '0',
        'exposed' => FALSE,
        'expose' => array(
          'operator' => FALSE,
          'label' => '',
        ),
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'relationship' => 'none',
      ),
      'is_product' => array(
        'operator' => '=',
        'value' => 1,
        'group' => '0',
        'exposed' => FALSE,
        'expose' => array(
          'operator' => FALSE,
          'label' => '',
        ),
        'id' => 'is_product',
        'table' => 'uc_products',
        'field' => 'is_product',
        'relationship' => 'none',
      ),
      'tid' => array(
        'operator' => 'or',
        'value' => array(
          0 => $tid_special_offers,
        ),
        'group' => '0',
        'exposed' => FALSE,
        'expose' => array(
          'operator' => FALSE,
          'label' => '',
        ),
        'type' => 'textfield',
        'limit' => TRUE,
        'vid' => $catalog_vid,
        'id' => 'tid',
        'table' => 'term_node',
        'field' => 'tid',
        'hierarchy' => 0,
        'relationship' => 'none',
        'reduce_duplicates' => 0,
      ),
    ));
    $handler->override_option('title', t('Special offers'));
    $handler->override_option('items_per_page', 2);
    $handler->override_option('path', 'homepage');
    $handler->override_option('menu', array(
      'type' => 'none',
      'title' => '',
      'description' => '',
      'weight' => 0,
      'name' => 'navigation',
    ));
    $handler->override_option('tab_options', array(
      'type' => 'none',
      'title' => '',
      'description' => '',
      'weight' => 0,
      'name' => 'navigation',
    ));
    $handler = $view->new_display('block', t('Block'), 'block_1');
    $handler->override_option('title', t('New Products'));
    $handler->override_option('block_description', '');
    $handler->override_option('block_caching', -1);

    // Save views
    $view->save();

    // Set front page
    variable_set('site_frontpage', 'homepage');
  }
}

/**
 * Create default book page
 */
function uberpaltw_setup_book() {
  // Insert service book
  $node = new stdClass();
  $node->type = 'book';
  $node->title = '客服中心';
  $node->body = "您可在此了解我們對於商品的運費計算、退換貨原則等資訊。";
  $node->format = 1;
  $node->book['bid'] = 'new';
  $node->path = 'service';
  $node->created = time();
  $node->changed = time();
  $node->uid = 1;
  $node->status = 1;
  $node->promote = 0;
  $node->sticky = 0;
  $node->language = 'zh-hant';
  node_save($node);

  // Get service bid, plid
  $service = db_fetch_object(db_query("SELECT b.mlid, b.bid FROM {node} n INNER JOIN {book} b ON b.nid=n.nid WHERE n.title='客服中心' AND type='book' LIMIT 1"));

  if ($service) {
    // Insert service return book
    $node = new stdClass();
    $node->type = 'book';
    $node->title = '運費計算';
    $node->body = "以下是我們的運費計算模式：";
    $node->format = 1;
    $node->book['bid'] = $service->bid;
    $node->book['plid'] = $service->mlid;
    $node->path = 'service/shipping';
    $node->created = time();
    $node->changed = time();
    $node->uid = 1;
    $node->status = 1;
    $node->promote = 0;
    $node->sticky = 0;
    $node->language = 'zh-hant';
    node_save($node);
    
    // Insert service shipping book
    $node = new stdClass();
    $node->type = 'book';
    $node->title = '退換貨原則';
    $node->body = "以下是我們的退換貨原則：";
    $node->format = 1;
    $node->book['bid'] = $service->bid;
    $node->book['plid'] = $service->mlid;
    $node->path = 'service/return';
    $node->created = time();
    $node->changed = time();
    $node->uid = 1;
    $node->status = 1;
    $node->promote = 0;
    $node->sticky = 0;
    $node->language = 'zh-hant';
    node_save($node);
  }

  // Insert about book
  $node = new stdClass();
  $node->type = 'book';
  $node->title = '關於我們';
  $node->body = "這裡是您的購物網站的介紹，以及聯絡方式的說明。";
  $node->format = 1;
  $node->book['bid'] = 'new';
  $node->path = 'about';
  $node->created = time();
  $node->changed = time();
  $node->uid = 1;
  $node->status = 1;
  $node->promote = 0;
  $node->sticky = 0;
  $node->language = 'zh-hant';
  node_save($node);
}

/**
 * Configure the blocks default settings
 */
function uberpaltw_setup_blocks() {
  // Insert login readme block
  db_query("INSERT INTO {boxes} (body, info, format) VALUES ('%s', '%s', %d)", '若您曾購買我們的商品，可登入網站以檢視訂單明細。', '會員專區登入說明', 1);
  $delta_nav = db_last_insert_id('boxes', 'bid');
  db_query("INSERT INTO {blocks_roles} (rid, module, delta) VALUES (%d, '%s', '%s')", 1, 'block', $delta_nav);

  $theme_acquia = false;
  if (variable_get('theme_default', '') == 'acquia_prosper') {
    $theme_acquia = true;
  }

  // Enable block
  $blocks = array(
    'readme' => array('module' => 'block', 'delta' => $delta_nav, 'weight' => -10, 'region' => 'left', 'visibility' => 1, 'pages' => 'user', 'title' => '登入說明'),
    'cart' => array('module' => 'uc_cart', 'delta' => 0, 'weight' => -5, 'region' => 'left', 'pages' => 'cart/checkout*'),
    'login' => array('module' => 'user', 'delta' => 0, 'weight' => -4, 'region' => 'sidebar_last', 'pages' => 'cart/checkout*'),
    'nav' => array('module' => 'user', 'delta' => 1, 'weight' => -3, 'region' => 'sidebar_last', 'pages' => 'cart/checkout*'),
    'products' => array('module' => 'views', 'delta' => 'products_home-block_1', 'weight' => 0, 'region' => 'content_bottom', 'visibility' => 1, 'pages' => '<front>'),
  );
  if ($theme_acquia) {
    $blocks['readme']['region'] = 'sidebar_last';
    $blocks['cart']['region'] = 'header';
  }
  else {
    unset($blocks['login'], $blocks['nav'], $blocks['products']);
  }

  foreach ($blocks as $block) {
    if ($theme_acquia) {
      $block['theme'] = 'acquia_prosper';
    }
    else {
      $block['theme'] = 'garland';
    }
    $block['status'] = 1;
    drupal_write_record('blocks', $block);
  }

  // Setting block
  // Hide cart block if cart is empty.
  if (!$theme_acquia) {
    variable_set('uc_cart_block_empty_hide', true);
  }

  if ($theme_acquia) {
    $settings = array(
      'block' => array(
        'uc_catalog-0' => array(
          'prosper-general-styles' => 'prosper-gray-rounded-style prosper-rounded-title',
        ),
        'uc_cart-0' => array(
          'grid16-width' => 'grid16-5',
          'fusion-alignment' => 'fusion-right',
          'prosper-general-styles' => 'prosper-shoppingcart-dark',
        ),
        'menu-primary-links' => array(
          'fusion-menu' => 'fusion-inline-menu',
        ),
      ),
    );
    variable_set('skinr_acquia_prosper', $settings);
  }
}

/**
 * Add admin role and setup default permission
 */
function uberpaltw_setup_user() {
  // Add administrator role
  db_query("INSERT INTO {role} (name) VALUES ('%s')", 'admin');

  // Get administrator rid
  $role = db_fetch_object(db_query("SELECT rid FROM {role} WHERE name='%s' LIMIT 1", 'admin'));

  // Setup admin permission
  if ($role) {
    $perm = array(
      'access printer-friendly version', 'add content to books', 'administer book outlines', 'create new books', // book
      'administer conditional actions', // ca
      'access comments', 'post comments', 'post comments without approval', // comment
      'access content', 'create book content', 'delete own book content', 'edit own book content', // node
      'access administration pages', // system
      'administer catalog', 'view catalog', // uc_catalog
      'administer credit cards', 'process credit cards', 'view cc details', 'view cc numbers', // uc_credit
      'administer order workflow', 'create orders', 'delete orders', 'edit orders', 'view all orders', 'view own orders', // uc_order
      'delete payments', 'manual payments', 'view payments', // uc_payment
      'administer own product features', 'administer product classes', 'administer product features', 'administer products', 'create products', 'delete all products', 'delete own products', 'edit all products', 'edit own products', // uc_product
      'administer store', 'view customers', 'view store reports', // uc_store
    );
    db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", $role->rid, implode(', ', $perm));
  }

  // Setup anonymous permission
  $perm = array(
    'view catalog', // uc_catalog
    'view own orders', // uc_order
  );
  db_query("UPDATE {permission} SET perm = CONCAT(perm, '%s') WHERE pid = 1", ', ' . implode(', ', $perm));

  // Setup authenticated permission
  $perm = array(
    'view catalog', // uc_catalog
    'view own orders', // uc_order
  );
  db_query("UPDATE {permission} SET perm = CONCAT(perm, '%s') WHERE pid = 2",  ', ' . implode(', ', $perm));

  // Add root user role
  db_query("INSERT INTO {users_roles} (uid, rid) VALUES (%d, %d)", 1, 3);
}

/**
 * Setup default menu items
 */
function uberpaltw_setup_menu() {
  $items = array(
    array('link_path' => '<front>', 'link_title' => t('Home'), 'weight' => 1),
    array('link_path' => 'catalog', 'link_title' => t('Catalog'), 'weight' => 2),
    array('link_path' => 'user', 'link_title' => t('Member'), 'weight' => 3),
    
  );

  // Add service menu item
  if ($service = db_fetch_object(db_query("SELECT nid FROM {node} n WHERE n.title='客服中心' AND type='book' LIMIT 1"))) {
    $items[] = array('link_path' => 'node/' . $service->nid, 'link_title' => t('Service'), 'weight' => 4);
  }

  // Add about menu item
  if ($about = db_fetch_object(db_query("SELECT nid FROM {node} n WHERE n.title='關於我們' AND type='book' LIMIT 1"))) {
    $items[] = array('link_path' => 'node/' . $about->nid, 'link_title' => t('About'), 'weight' => 5);
  }

  foreach ($items as $item) {
    $item += array(
      'mlid' => 0,
      'module' => 'menu',
      'has_children' => 0,
      'options' => array(
        'attributes' => array(
          'title' => '',
        ),
      ),
      'customized' => 1,
      'original_item' => array(
        'link_title' => '',
        'mlid' => 0,
        'plid' => 0,
        'menu_name' => 'primary-links',
        'weight' => 1,
        'link_path' => '',
        'options' => array(),
        'module' => 'menu',
        'expanded' => 0,
        'hidden' => 0,
        'has_children' => 0,
      ),
      'description' => '',
      'expanded' => 0,
      'parent' => 'primary-links:0',
      'hidden' => 0,
      'plid' => 0,
      'menu_name' => 'primary-links',
    );
    menu_link_save($item);
  }
}
